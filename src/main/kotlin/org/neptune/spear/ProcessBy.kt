package org.neptune.spear

import com.sun.jna.Platform
import org.neptune.spear.windows.Windows
import java.util.*

fun processByID(processID: Int): Process? = when {
    Platform.isWindows() || Platform.isWindowsCE() -> Windows.openProcess(processID)
    else -> null
}

fun processByName(processName: String): Process? = when {
    Platform.isWindows() || Platform.isWindowsCE() -> Windows.openProcess(processName)
    else -> null
}